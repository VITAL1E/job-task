package vitalik;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;


public class FileReader {

    public static int recordProcessedCounter;
    public static int recordSuccessfulCounter;
    public static int recordFailedCounter;

    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {

        String fileLocation = File.separator + "Users" + File.separator + "Vitalie" + File.separator + "IdeaProjects" + File.separator + "Job-Task" + File.separator + "src" + File.separator + "vitalik" + File.separator + "records.csv";

        try {
            Class.forName("org.sqlite.JDBC");
            System.out.println("Load driver success");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Vitalie\\IdeaProjects\\Job-Task\\database.sqlite");

            // Query insert to table record with 10 columns
            String query = "INSERT INTO RECORD (a, b, c, d, e, f, g, h, i, j) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            // Create prepare statement
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            // Get list record from file
            ArrayList<Record> listRecord = getListRecordFromFile(fileLocation);

            // Insert list to database
            for (int i = 0; i < listRecord.size(); i++) {

                preparedStatement.setString(1, listRecord.get(i).getA());
                preparedStatement.setString(2, listRecord.get(i).getB());
                preparedStatement.setString(3, listRecord.get(i).getC());
                preparedStatement.setString(4, listRecord.get(i).getD());
                preparedStatement.setString(5, listRecord.get(i).getE());
                preparedStatement.setString(6, listRecord.get(i).getF());
                preparedStatement.setString(7, listRecord.get(i).getG());
                preparedStatement.setString(8, listRecord.get(i).getH());
                preparedStatement.setString(9, listRecord.get(i).getI());
                preparedStatement.setString(10, listRecord.get(i).getJ());

                preparedStatement.executeUpdate();
                recordSuccessfulCounter++;
                System.out.println("Insert success record: " + (i + 1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(recordProcessedCounter + " records received");
        System.out.println(recordSuccessfulCounter + " records successful");
        System.out.println(recordFailedCounter + " records failed");
    }


    public static ArrayList<Record> getListRecordFromFile(String filePath) throws IOException {
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        ArrayList<Record> listRecord = new ArrayList<>();
        FileOutputStream fileOutputStream = new FileOutputStream("timestamp");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        try {
            fileInputStream = new FileInputStream(filePath);
            inputStreamReader = new InputStreamReader(fileInputStream);
            bufferedReader = new BufferedReader(inputStreamReader);

            // String save line get from text file
            String line = null;

            // Array save record
            String[] stringRecord = null;

            // Loop and get all data in text file
            while (true) {

                // Get 1 line
                line = bufferedReader.readLine();
                recordProcessedCounter++;

                // Check line, exit loop if empty
                if (line == null) {
                    break;
                } else {
                     stringRecord = line.split(",");

                        // Check if record has right number of columns
                        if (!stringRecord[0].isEmpty() && !stringRecord[1].isEmpty() && !stringRecord[2].isEmpty() && !stringRecord[3].isEmpty() && !stringRecord[4].isEmpty() && !stringRecord[5].isEmpty() && !stringRecord[6].isEmpty() && !stringRecord[7].isEmpty() && !stringRecord[8].isEmpty() && !stringRecord[9].isEmpty()) {
                            listRecord.add(new Record(stringRecord[0], stringRecord[1], stringRecord[2], stringRecord[3], stringRecord[4], stringRecord[5], stringRecord[6], stringRecord[7], stringRecord[8], stringRecord[9]));
                        } else {

                            // Add bad record to file
                            objectOutputStream.writeObject(stringRecord);
                            recordFailedCounter++;
                        }
                }
            }

            objectOutputStream.close();

        } catch (Exception e) {
            System.out.println("Read file error");
            e.printStackTrace();
        } finally {

            // Close file
            try {
                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return listRecord;
    }
}
